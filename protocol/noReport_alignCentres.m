
% Align two stimuli, presented each to the right and left visual fields, so
% that they merge into one under stereoscopic vewing conditions. This will
% save a file that will be later used for the actual experiment where we
% present the binocular images

clear all  %#ok<CLALL>

global cfg;
global dev;


rootPath        = '~/git/noReport_metacognition/';
cfg.dataPath    = '/Data_temp/';
functionsPath   = '/functions/';
mkdir([rootPath cfg.dataPath])
addpath(rootPath)
addpath([rootPath functionsPath])

cfg.debug = 1;

commandwindow;

%% get subject data
dlg = inputdlg({'Pseudonym', 'Age', 'Sex f/m', 'Screen height (cm)', 'Screen height (pixels)'},'Input');
DATA.info.pseudonym     = dlg{1};
DATA.info.age           = str2double(dlg{2});
DATA.info.sex           = dlg{3};
dev.screenHeightCm      = str2double(dlg{4});
dev.screenHeightPixels  = str2double(dlg{5});
if isempty(DATA.info.pseudonym), DATA.info.pseudonym = 'test'; end
if isnan(DATA.info.age),         DATA.info.age       = NaN;    end
if isempty(DATA.info.sex),       DATA.info.sex       = '?';    end
if isnan(dev.screenHeightCm),    dev.screenHeightCm  = 33.6;   end
if isnan(dev.screenHeightPixels),dev.screenHeightPixels = 1440;end          %Defaults to large Dell Screens
cfg.dataPathSubj        = [rootPath cfg.dataPath DATA.info.pseudonym];

cfg.dataPathSubj = [rootPath cfg.dataPath DATA.info.pseudonym];
mkdir(cfg.dataPathSubj);



%% Determine key for responses

KbName('UnifyKeyNames')
cfg.moveKeys   = [KbName('RightArrow') KbName('LeftArrow') KbName('space')];

cfg.step = .5;      %How quickly (and finely) the stimuli move 

%TODO: (discuss with Nathan) should we vary stim size based on viewing angle? Seems annoying to
%have to adjust the little square in the stimuli to the actual required size, and 
% potentially unecessary
cfg.viewingDistance    = 60;    % Distance between monitor and participant in cm

cfg.midPointSize = 2; %the (half-)size in pixels of the little circle in the middle of each square


%% open screen

screens = Screen('Screens');
dev.screenNumber = max(screens);

% Get RGB values for white, grey, black (overdoing it, probably)
dev = findColorValues(dev);


if cfg.debug    
    Screen('Preference', 'SkipSyncTests', 1);
    PsychDebugWindowConfiguration;
else
    Screen('Preference', 'SkipSyncTests', 0);
end
[dev.wnd, dev.screenRect] = Screen('OpenWindow', dev.screenNumber, dev.black);


% get the x centre
dev.centreScreenX       = (dev.screenRect(3) - dev.screenRect(1))/2;
%get the y centre
dev.centreScreenY       = (dev.screenRect(4) - dev.screenRect(2))/2;
% and the centres of the two halves (positions of the two fix crosses)
dev.centreScreenLeftX   = (dev.centreScreenX - dev.screenRect(1)) / 2;
dev.centreScreenRightX  = (dev.screenRect(3) - dev.centreScreenX ) / 2 + dev.centreScreenX ;

Screen('TextSize', dev.wnd, 24);
cfg.textColor = dev.white;


%% Display instructions

instructions = 'Align the squares. \n\nPress Space bar when done. \n\nPress any key to begin';

DrawFormattedText(dev.wnd, instructions, 'center', 'center', dev.white);
Screen('Flip', dev.wnd);

KbWait();

%% Draw stimuli to respond to key presses

%initialize
centreLeftAdjusted  = dev.centreScreenLeftX;
centreRightAdjusted = dev.centreScreenRightX;

RestrictKeysForKbCheck(cfg.moveKeys);
FlushEvents;

happy = 0;
while ~happy
    
    FlushEvents;
    
    drawCentreMatchingStimuli(centreLeftAdjusted, centreRightAdjusted);
    
    [keyIsDown, secs, keyCode] = KbCheck(-1); %query all devices
    
    if strcmp(KbName(keyCode),'RightArrow')
        centreLeftAdjusted  = centreLeftAdjusted + cfg.step;
        centreRightAdjusted = centreRightAdjusted - cfg.step;
        
    elseif strcmp(KbName(keyCode),'LeftArrow')
        centreLeftAdjusted  = centreLeftAdjusted - cfg.step;
        centreRightAdjusted = centreRightAdjusted + cfg.step;
        
    elseif strcmp(KbName(keyCode),'space')
        happy = 1;
    end
    
end

DATA.align.centreLeftAdjusted = centreLeftAdjusted;
DATA.align.centreRightAdjusted = centreRightAdjusted;

save([cfg.dataPathSubj '/noReport_alignCentres.mat']);

RestrictKeysForKbCheck([])

Screen('CloseAll')

sprintf('%s%g', 'adjusted left is ', centreLeftAdjusted)
sprintf('%s%g', 'adjusted right is ', centreRightAdjusted)




