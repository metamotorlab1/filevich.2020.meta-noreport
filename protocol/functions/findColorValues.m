function dev = findColorValues(dev)

% Find the color values which correspond to white and black: Usually
% black is always 0 and white 255, but this rule is not true if one of
% the high precision framebuffer modes is enabled via the
% PsychImaging() commmand, so we query the true values via the
% functions WhiteIndex and BlackIndex:
white = WhiteIndex(dev.screenNumber);
black = BlackIndex(dev.screenNumber);

% Round gray to integral number, to avoid roundoff artifacts with some
% graphics cards:
gray = round((white+black)/2);

% This makes sure that on floating point framebuffers we still get a
% well defined gray. It isn't strictly neccessary in this demo:
if gray == white
    gray = white / 2;
end

% Contrast 'inc'rement range for given white and gray values:
inc = white - gray;

dev.gray  = gray;
dev.grey  = gray; %happens all the time
dev.white = white;
dev.black = black;
dev.inc   = inc;

return