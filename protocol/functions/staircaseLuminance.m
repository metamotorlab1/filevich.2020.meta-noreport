function DATA = staircaseLuminance(b,t,DATA)

global cfg;

% Get next trial and block index
if t < cfg.ntrials
    tnext = t+1;
    bnext = b;
elseif t == cfg.ntrials && b < cfg.nblocks
    tnext = 1;
    bnext = b+1;
elseif t == cfg.ntrials && b == cfg.nblocks
    tnext = [];
end

%Staircase is very simple because we just want to reach 50%
if ~isempty(tnext)
    if isempty(DATA.continuousReport(b,t).reportedDominance)
        DATA.continuousReport(bnext,tnext).redLuminance   = DATA.continuousReport(b,t).redLuminance;
        DATA.continuousReport(bnext,tnext).greenLuminance = DATA.continuousReport(b,t).greenLuminance;
    else
        if DATA.continuousReport(b,t).reportedDominance.greenSecs/DATA.continuousReport(b,t).reportedDominance.redSecs > 3/2
            %decrease green, increase red
            DATA.continuousReport(bnext,tnext).redLuminance   = DATA.continuousReport(b,t).redLuminance   + cfg.luminanceStep;
            DATA.continuousReport(bnext,tnext).greenLuminance = DATA.continuousReport(b,t).greenLuminance - cfg.luminanceStep;
            
        elseif DATA.continuousReport(b,t).reportedDominance.greenSecs/DATA.continuousReport(b,t).reportedDominance.redSecs < 2/3
            %increase green, decrease red
            DATA.continuousReport(bnext,tnext).redLuminance   = DATA.continuousReport(b,t).redLuminance   - cfg.luminanceStep;
            DATA.continuousReport(bnext,tnext).greenLuminance = DATA.continuousReport(b,t).greenLuminance + cfg.luminanceStep;
        else
            %do nothing
            DATA.continuousReport(bnext,tnext).redLuminance   = DATA.continuousReport(b,t).redLuminance;
            DATA.continuousReport(bnext,tnext).greenLuminance = DATA.continuousReport(b,t).greenLuminance;
        end
    end
end


% TODO Need stopping rule

end