function gotoNextTrial(b,t)

global dev;
global cfg;

% figure out what's going on - too much but we might want to change
if t < cfg.ntrials
    continueKey = cfg.startKey;
elseif t == cfg.ntrials && b < cfg.nblocks
    continueKey = cfg.startKey;
elseif t == cfg.ntrials && b == cfg.nblocks
    continueKey = cfg.startKey;
end

if strcmp(cfg.allInputs, 'keyboard')
    continueKeyName = KbName(continueKey);
elseif strcmp(cfg.allInputs, 'gamepad')
    continueKeyName = dev.gamepadButtonNames{continueKey};
end

if t < cfg.ntrials
    continueMessage = ['Press ' continueKeyName ' to start next trial'];
elseif t == cfg.ntrials && b < cfg.nblocks
    continueMessage = ['Block is over! Take a break\nPress ' continueKeyName ' to continue'];
elseif t == cfg.ntrials && b == cfg.nblocks
    continueMessage = ['Experiment is over!\nPress ' continueKeyName ' to finish'];
end

DrawFormattedText(dev.wnd, continueMessage, dev.centreScreenX, 'center', dev.white);
Screen('Flip', dev.wnd);

PsychHID('KbQueueStart', dev.inputRivalryIndex);
pressedKey = 0;
while ~pressedKey
    [pressedKey, firstPress] = PsychHID('KbQueueCheck', dev.inputRivalryIndex);
    if ~ismember(find(firstPress), cfg.startKey)
        pressedKey = 0;
    end
end
PsychHID('KbQueueStop', dev.inputRivalryIndex);
PsychHID('KbQueueFlush', dev.inputRivalryIndex);

end







