function [cfg, dev, allDevs] = setKeys4Responses(cfg, dev)

allDevs = PsychHID('Devices');

if strcmp(cfg.allInputs, 'gamepad')
    cfg.inputRivalry = 'gamepad';
    cfg.inputProbe   = 'gamepad';
    cfg.inputType1   = 'gamepad';
    cfg.inputVAS     = 'gamepad';
    dev.gamepadIndex = Gamepad('GetNumGamepads');       %will be 0 if not connected
    
elseif strcmp(cfg.allInputs, 'keyboard')
    cfg.inputRivalry = 'keyboard';
    cfg.inputProbe   = 'keyboard';
    cfg.inputType1   = 'keyboard';
    cfg.inputVAS     = 'mouse';
    KbName('UnifyKeyNames')
end

dev.gamepadButtonNames = {'X', 'A', 'B', 'Y', 'LB', 'RB', 'LT', 'RT', 'Back', 'Start', 'LeftThumbstickDown', 'RightThumbstickDown'};
dev.axesNames = {'left', 'left', 'right', 'right'};

dev.gamepadYmax     = 32767;
dev.gamepadYmin     = -32768;
dev.gamepadYnegZero = -129;
dev.gamepadYposZero = 128;

%% Rivalry (continuous report)
if strcmp(cfg.inputRivalry, 'keyboard')
    
    %% Config continuous report and Type 1 question
    dev.inputRivalryIndex = structfind(allDevs, 'usageName', 'Keyboard');
    
    cfg.rivalryResponseKeyRed   = KbName('K');
    cfg.rivalryResponseKeyGreen = KbName('L');
    cfg.type1ResponseKeyRed     = cfg.rivalryResponseKeyRed;
    cfg.type1ResponseKeyGreen   = cfg.rivalryResponseKeyGreen;
    
    cfg.keyAttentionProbe1 = KbName('A'); 
    cfg.keyAttentionProbe2 = KbName('S'); 
    cfg.startKey           = KbName('space');
    
    %% VAS
    cfg.confirmConfidenceKey = 1;        % Left mouse button
    
    %% Phrase the questions
    cfg.type1Question   = ['Which percept dominated?\n\n ' ...
        KbName(cfg.type1ResponseKeyGreen) ' or ' ...
        KbName(cfg.type1ResponseKeyRed)];
    cfg.expInstructions = ['Press ' KbName(cfg.startKey) ' to start experiment.' ...
        '\n\nFilled circles mean continuous report with ' KbName(cfg.type1ResponseKeyGreen) ' or ' KbName(cfg.type1ResponseKeyRed) '.' ...
        '\n\nEmpty circles mean no report.' ...
        '\n\n' ...
        '\n\nIf you see ' cfg.attentionProbeID(1) ' press ' KbName(cfg.keyAttentionProbe1) ...
        '\n\nIf you see ' cfg.attentionProbeID(2) ' press ' KbName(cfg.keyAttentionProbe2) '.'];
    %But overwrite if necessary
    if cfg.doStaircase
        cfg.expInstructions = ['Press ' KbName(cfg.startKey) ' to start experiment.' ...
        '\n\nReport the dominant percept with ' KbName(cfg.type1ResponseKeyGreen) ...
        ' or ' KbName(cfg.type1ResponseKeyRed) '.'];
    end
    cfg.expInstructions_startOnly = ['Press ' KbName(cfg.startKey) ' to start experiment.'];
    
    
elseif strcmp(cfg.inputRivalry, 'gamepad')
    
    %% Continuous report
    dev.inputRivalryIndex = structfind(allDevs, 'usageName', 'Joystick');
    
    cfg.rivalryResponseKeyRed   = 3;        %red button (B)
    cfg.rivalryResponseKeyGreen = 2;        %green button (A)
    cfg.type1ResponseKeyRed     = cfg.rivalryResponseKeyRed;
    cfg.type1ResponseKeyGreen   = cfg.rivalryResponseKeyGreen;
    
    cfg.attentionProbeAxisIndex = 1;        %left thumbstick horizontal
    cfg.keyAttentionProbe1      = dev.gamepadYmin/2;
    cfg.keyAttentionProbe1Name  = 'leftwards';
    cfg.keyAttentionProbe2      = dev.gamepadYmax/2;
    cfg.keyAttentionProbe2Name  = 'rightwards';
    
    cfg.startKey                = 7;             %Left hand, LT button (back of controller)
    
    %% VAS
    cfg.confirmConfidenceKey    = 12;    %right thumbstick down
    cfg.VASaxisIndex            = 4;
    
    %% Phrase the questions
    cfg.type1Question   = ['Which percept dominated?\n\n Green (' ...
        dev.gamepadButtonNames{cfg.type1ResponseKeyGreen} ...
        ') or Red (' dev.gamepadButtonNames{cfg.type1ResponseKeyRed} ')'];
    cfg.expInstructions = ['Press ' dev.gamepadButtonNames{cfg.startKey} ' to start experiment.' ...
        '\n\nFilled circles mean continuous report with ' dev.gamepadButtonNames{cfg.type1ResponseKeyGreen} ' or ' dev.gamepadButtonNames{cfg.type1ResponseKeyRed} '.' ...
        '\n\nEmpty circles mean no report.' ...
        '\n\n' ...
        '\n\nIf you see ' cfg.attentionProbeID(1) ' move ' dev.axesNames{cfg.attentionProbeAxisIndex} 'thumb ' cfg.keyAttentionProbe1Name '.' ...
        '\n\nIf you see ' cfg.attentionProbeID(2) ' move ' dev.axesNames{cfg.attentionProbeAxisIndex} 'thumb ' cfg.keyAttentionProbe2Name '.'];
   
    if cfg.doStaircase
       cfg.expInstructions = ['Press ' dev.gamepadButtonNames{cfg.startKey} ' to start experiment.' ...
        '\n\nReport the dominant percept with ' dev.gamepadButtonNames{cfg.type1ResponseKeyGreen} ...
        ' or ' dev.gamepadButtonNames{cfg.type1ResponseKeyRed} '.'];
    end
    
    cfg.expInstructions_startOnly = ['Press ' dev.gamepadButtonNames{cfg.startKey} ' to start experiment.'];

    
    
end

end


