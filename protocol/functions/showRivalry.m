function DATA = showRivalry(dynamicGrating, b, t, DATA)
% Adapted from Psychtoolbox's DriftDemo2 
% http://docs.psychtoolbox.org/DriftDemo2

global dev;
global cfg;


%recover shorter variable names
visiblesize     = dynamicGrating.visiblesize;
masktex         = dynamicGrating.masktex;
%gratingtex      = dynamicGrating.gratingtex;    %this one is monochrome
gratingtexRed   = dynamicGrating.gratingtexRed;
gratingtexGreen = dynamicGrating.gratingtexGreen;


dstRect       = dynamicGrating.dstRect;
% dstRect is the destination rect, centered in the middle of the screen.
% It's Defined in createGratingTexture.m. We need to center it to the two
% new (left/right) centres for the binocular stimuli.
dstRectLeft  = CenterRectOnPoint(dstRect, DATA.centreLeftAdjusted,  dev.centreScreenY);
dstRectRight = CenterRectOnPoint(dstRect, DATA.centreRightAdjusted, dev.centreScreenY);

f               = cfg.cyclesPerPixel;
cyclespersecond = cfg.cyclesPerSecond;

% Query duration of one monitor refresh interval:
ifi = Screen('GetFlipInterval', dev.wnd);

% Translate that into the amount of seconds to wait between screen
% redraws/updates:

% waitframes = 1 means: Redraw every monitor refresh. If your GPU is
% not fast enough to do this, you can increment this to only redraw
% every n'th refresh. All animation paramters will adapt to still
% provide the proper grating. However, if you have a fine grating
% drifting at a high speed, the refresh rate must exceed that
% "effective" grating speed to avoid aliasing artifacts in time, i.e.,
% to make sure to satisfy the constraints of the sampling theorem
% (See Wikipedia: "Nyquist?Shannon sampling theorem" for a starter, if
% you don't know what this means):
waitframes = 1;

% Translate frames into seconds for screen update interval:
waitduration = waitframes * ifi;

% Recompute p, this time without the ceil() operation from the createGratingTexture case.
% Otherwise we will get wrong drift speed due to rounding errors!
p = 1/f;  % pixels/cycle

% Translate requested speed of the grating (in cycles per second) into
% a shift value in "pixels per frame", for given waitduration: This is
% the amount of pixels to shift our srcRect "aperture" in horizontal
% directionat each redraw:
shiftperframe = cyclespersecond * p * waitduration;

%show cues to indicate the condition (continuous report/no report: filled/empty circles)
rectsForOvals = cat(2,[DATA.centreLeftAdjusted - cfg.conditionCuePositionX - cfg.conditionCueSize; dstRectLeft(2) + cfg.conditionCuePositionY + cfg.conditionCueSize; ...
    DATA.centreLeftAdjusted - cfg.conditionCuePositionX + cfg.conditionCueSize; dstRectLeft(2) + cfg.conditionCuePositionY - cfg.conditionCueSize], ...
    [DATA.centreLeftAdjusted + cfg.conditionCuePositionX - cfg.conditionCueSize; dstRectLeft(2) + cfg.conditionCuePositionY + cfg.conditionCueSize; ...
    DATA.centreLeftAdjusted + cfg.conditionCuePositionX + cfg.conditionCueSize; dstRectLeft(2) + cfg.conditionCuePositionY - cfg.conditionCueSize]);
penwidth = 2;
if DATA.trialDef.reportTrial(b,t) == 1
    Screen('FillOval', dev.wnd, dev.white, rectsForOvals);
elseif DATA.trialDef.reportTrial(b,t) == 0
    Screen('FrameOval', dev.wnd, dev.white, rectsForOvals, penwidth);
end
conditionCueFlip = Screen('Flip', dev.wnd);

while GetSecs < (conditionCueFlip + cfg.conditionCueDuration)
end

% Start the queue to get keypresses
PsychHID('KbQueueStart', dev.inputRivalryIndex);
%Initialize:
DATA.continuousReport(b,t).keyIDpress   = [];
DATA.continuousReport(b,t).keyPressTime = [];
DATA.continuousReport(b,t).keyIDrelease = [];
DATA.continuousReport(b,t).keyReleaseTime = [];

report.keypressIndex    = 0;          %cumulative over a single trial
report.keyreleaseIndex  = 0;
report.mouseStatus      = 'notpressed';
report.probeIndex       = 1;

% Perform initial Flip to sync us to the VBL and for getting an initial
% VBL-Timestamp as timing baseline for our redraw loop:
vbl = Screen('Flip', dev.wnd);
rivalryStimOnset = vbl;

vblendtime = rivalryStimOnset + DATA.trialDef.trialDuration(b,t);
counter = 0;

% Animationloop:
while(vbl < vblendtime)
    
    % Shift the grating by "shiftperframe" pixels per frame:
    % the mod'ulo operation makes sure that our "aperture" will snap
    % back to the beginning of the grating, once the border is reached.
    % Fractional values of 'xoffset' are fine here. The GPU will
    % perform proper interpolation of color values in the grating
    % texture image to draw a grating that corresponds as closely as
    % technical possible to that fractional 'xoffset'. GPU's use
    % bilinear interpolation whose accuracy depends on the GPU at hand.
    % Consumer ATI hardware usually resolves 1/64 of a pixel, whereas
    % consumer NVidia hardware usually resolves 1/256 of a pixel. You
    % can run the script "DriftTexturePrecisionTest" to test your
    % hardware...
    xoffset = mod(counter * shiftperframe, p);
    counter = counter + 1;
    
    % Define shifted srcRect that cuts out the properly shifted rectangular
    % area from the texture: We cut out the range 0 to visiblesize in
    % the vertical direction although the texture is only 1 pixel in
    % height! This works because the hardware will automatically
    % replicate pixels in one dimension if we exceed the real borders
    % of the stored texture. This allows us to save storage space here,
    % as our 2-D grating is essentially only defined in 1-D:
    srcRect = [xoffset 0 xoffset + visiblesize visiblesize];
    
    % Draw green and red grating textures, rotated by cfg.tiltFromVertical:
    switch cfg.color2EyeMap
        case 1
            Screen('DrawTexture', dev.wnd, gratingtexGreen, srcRect, dstRectLeft, cfg.tiltFromVertical);
            Screen('DrawTexture', dev.wnd, gratingtexRed, srcRect,  dstRectRight, cfg.tiltFromVertical+180); %rotate 180 deg - easiest way to invert the direction of the stimuli
        case 2
            Screen('DrawTexture', dev.wnd, gratingtexRed, srcRect, dstRectLeft, cfg.tiltFromVertical);
            Screen('DrawTexture', dev.wnd, gratingtexGreen, srcRect,  dstRectRight, cfg.tiltFromVertical+180);
    end
    
    if cfg.drawmask == 1
        % Draw gaussian mask over grating:
        Screen('DrawTexture', dev.wnd, masktex, [0 0 visiblesize visiblesize], dstRect, cfg.tiltFromVertical);
    end;
    
    %show cues again to remind subject of the condition
    if DATA.trialDef.reportTrial(b,t) == 1
        Screen('FillOval', dev.wnd, dev.white, rectsForOvals);
    elseif DATA.trialDef.reportTrial(b,t) == 0
        Screen('FrameOval', dev.wnd, dev.white, rectsForOvals, penwidth);
    end
    
    %Draw the attentional Probe if the trial and timing are right
    if DATA.trialDef.attentionalProbe(b,t)
        %because this is a vector of times, check if we're close enough to
        %any of the times of the probe
        if any(abs((GetSecs - rivalryStimOnset) - DATA.trialDef.poissonTimes{b,t}) < cfg.attentionalProbeDuration)
            report.probeIndex = find( abs((GetSecs - rivalryStimOnset) - DATA.trialDef.poissonTimes{b,t}) < cfg.attentionalProbeDuration);
            DrawFormattedText(dev.wnd, cfg.attentionProbeID(DATA.trialDef.attentionalProbeShownID{b,t}(report.probeIndex)), DATA.centreLeftAdjusted, dev.centreScreenY, dev.white);
            DrawFormattedText(dev.wnd, cfg.attentionProbeID(DATA.trialDef.attentionalProbeShownID{b,t}(report.probeIndex)), DATA.centreRightAdjusted, dev.centreScreenY, dev.white);
            
        end
    end
    
    % Flip 'waitframes' monitor refresh intervals after last redraw.
    % Providing this 'when' timestamp allows for optimal timing
    % precision in stimulus onset, a stable animation framerate and at
    % the same time allows the built-in "skipped frames" detector to
    % work optimally and report skipped frames due to hardware
    % overload:
    vbl = Screen('Flip', dev.wnd, vbl + (waitframes - 0.5) * ifi);
    
    [DATA, report] = collectContinuousReport(DATA, b, t, rivalryStimOnset, report);
end

PsychHID('KbQueueStop', dev.inputRivalryIndex);
PsychHID('KbQueueFlush', dev.inputRivalryIndex);

%% store data
DATA.continuousReport(b,t).rivalryStimOnset = rivalryStimOnset;
if isempty(DATA.continuousReport(b,t).keyIDpress)
    DATA.continuousReport(b,t).keyIDpress     = [];
    DATA.continuousReport(b,t).keyPressTime   = [];
end
if isempty(DATA.continuousReport(b,t).keyIDrelease)
    DATA.continuousReport(b,t).keyIDrelease   = [];
    DATA.continuousReport(b,t).keyReleaseTime = [];
end

end

