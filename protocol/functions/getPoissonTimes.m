
function poissonTimes = getPoissonTimes(b, t)

global DATA;
global cfg;

% Adapted from http://www.hms.harvard.edu/bss/neuro/bornlab/nb204/statistics/poissonTutorial.txt
% Check their rasterPlot function to understand this one best

timeStepSecs = 0.1;   
eventsPerSec = cfg.attentionProbesPerTrial/DATA.trialDef.trialDuration(b,t);   
% Take the duration of the trial but leave a margin after trial onset and before trial offset where no probe will happen
durationSecs = DATA.trialDef.trialDuration(b,t) - 2 * cfg.timeMarginProbe;                  
times = 0 : timeStepSecs : durationSecs;	% a vector with each time step

%Get a random value, sampled independently for each time point 
vt = rand(size(times));
%Check if the the total number of events (events/sec * sec) exceeds the random value   
% The higher events/sec or the longer the time steps, the easier to get a 1 (i.e. an event in that time step)
poissonTimes = times((eventsPerSec*timeStepSecs) > vt);
poissonTimes  = poissonTimes  + cfg.timeMarginProbe; %correct for the margin at trial onset

end