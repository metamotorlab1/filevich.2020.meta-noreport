function DATA = showRivalry_explainTask(dynamicGrating, b, t, DATA)
% Adapted from http://docs.psychtoolbox.org/DriftDemo2

global dev;
global cfg;

%recover shorter variable names
visiblesize     = dynamicGrating.visiblesize;
masktex         = dynamicGrating.masktex;
gratingtexRed   = dynamicGrating.gratingtexRed;
gratingtexGreen = dynamicGrating.gratingtexGreen;

dstRect       = dynamicGrating.dstRect;
dstRectLeft  = CenterRectOnPoint(dstRect, DATA.centreLeftAdjusted,  dev.centreScreenY);
dstRectRight = CenterRectOnPoint(dstRect, DATA.centreRightAdjusted, dev.centreScreenY);

f               = cfg.cyclesPerPixel;
cyclespersecond = cfg.cyclesPerSecond;

% Query duration of one monitor refresh interval:
ifi = Screen('GetFlipInterval', dev.wnd);
waitframes = 1;
waitduration = waitframes * ifi;
p = 1/f;  % pixels/cycle
shiftperframe = cyclespersecond * p * waitduration;

%show cues to indicate the condition (continuous report/no report: filled/empty circles)
rectsForOvals = cat(2,[DATA.centreLeftAdjusted - cfg.conditionCuePositionX - cfg.conditionCueSize; dstRectLeft(2) + cfg.conditionCuePositionY + cfg.conditionCueSize; ...
    DATA.centreLeftAdjusted - cfg.conditionCuePositionX + cfg.conditionCueSize; dstRectLeft(2) + cfg.conditionCuePositionY - cfg.conditionCueSize], ...
    [DATA.centreLeftAdjusted + cfg.conditionCuePositionX - cfg.conditionCueSize; dstRectLeft(2) + cfg.conditionCuePositionY + cfg.conditionCueSize; ...
    DATA.centreLeftAdjusted + cfg.conditionCuePositionX + cfg.conditionCueSize; dstRectLeft(2) + cfg.conditionCuePositionY - cfg.conditionCueSize]);
penwidth = 2;

report.probeIndex       = 1;

vbl = Screen('Flip', dev.wnd);
rivalryStimOnset = vbl;
PsychHID('KbQueueStart', dev.inputKeyboardIndex);

vblendtime = rivalryStimOnset + DATA.trialDef.trialDuration(b,t);
counter = 0;
toggleProbes = 0; 
toggleReportCues = 0; 
toggleColour = 0;

% Animationloop:
while(vbl < vblendtime)
    
    [~, firstPress] = PsychHID('KbQueueCheck', dev.inputKeyboardIndex);
    whatToShow = find(firstPress);
    
    if ismember(KbName('p'), whatToShow) 
        toggleProbes = ~toggleProbes;
    end
    if ismember(KbName('r'), whatToShow) 
        toggleReportCues = ~toggleReportCues;
    end
    if ismember(KbName('ESCAPE'), whatToShow)
        break
    end
    if ismember(KbName('1!'), whatToShow) 
        toggleColour = 1;
    end
    if ismember(KbName('2@'), whatToShow) 
        toggleColour = 2;
    end
    if ismember(KbName('3#'), whatToShow) 
        toggleColour = 3;
    end
    
    xoffset = mod(counter * shiftperframe, p);
    counter = counter + 1;
    srcRect = [xoffset 0 xoffset + visiblesize visiblesize];
    
    % Draw green and red grating textures, rotated by cfg.tiltFromVertical:
    if toggleColour == 1
        switch cfg.color2EyeMap
            case 1
                Screen('DrawTexture', dev.wnd, gratingtexRed, srcRect, dstRectLeft, cfg.tiltFromVertical+180);
                Screen('DrawTexture', dev.wnd, gratingtexRed, srcRect, dstRectRight, cfg.tiltFromVertical+180);
            case 2
                Screen('DrawTexture', dev.wnd, gratingtexRed, srcRect, dstRectLeft, cfg.tiltFromVertical);
                Screen('DrawTexture', dev.wnd, gratingtexRed, srcRect, dstRectRight, cfg.tiltFromVertical);
        end
    elseif toggleColour == 2
        switch cfg.color2EyeMap
            case 1
                Screen('DrawTexture', dev.wnd, gratingtexGreen, srcRect, dstRectLeft, cfg.tiltFromVertical);
                Screen('DrawTexture', dev.wnd, gratingtexGreen, srcRect, dstRectRight, cfg.tiltFromVertical);
            case 2
                Screen('DrawTexture', dev.wnd, gratingtexGreen, srcRect, dstRectLeft, cfg.tiltFromVertical + 180);
                Screen('DrawTexture', dev.wnd, gratingtexGreen, srcRect, dstRectRight, cfg.tiltFromVertical + 180);
        end
    elseif toggleColour == 3
        switch cfg.color2EyeMap
            case 1
                Screen('DrawTexture', dev.wnd, gratingtexGreen, srcRect, dstRectLeft, cfg.tiltFromVertical);
                Screen('DrawTexture', dev.wnd, gratingtexRed, srcRect,  dstRectRight, cfg.tiltFromVertical+180); %rotate 180 deg - easiest way to invert the direction of the stimuli
            case 2
                Screen('DrawTexture', dev.wnd, gratingtexRed, srcRect, dstRectLeft, cfg.tiltFromVertical);
                Screen('DrawTexture', dev.wnd, gratingtexGreen, srcRect,  dstRectRight, cfg.tiltFromVertical+180); 
        end
    end
    
    %show cues again to remind subject of the condition
    if toggleReportCues
        Screen('FillOval', dev.wnd, dev.white, rectsForOvals);  %show instruction cues filled 
    else
        Screen('FrameOval', dev.wnd, dev.white, rectsForOvals, penwidth);   %or empty
    end
    
    %Draw the attentional Probe if the trial and timing are right
    if toggleProbes
        %because this is a vector of times, check if we're close enough to
        %any of the times of the probe
        if any(abs((GetSecs - rivalryStimOnset) - DATA.trialDef.poissonTimes{b,t}) < cfg.attentionalProbeDuration)
            report.probeIndex = find( abs((GetSecs - rivalryStimOnset) - DATA.trialDef.poissonTimes{b,t}) < cfg.attentionalProbeDuration);
            DrawFormattedText(dev.wnd, cfg.attentionProbeID(DATA.trialDef.attentionalProbeShownID{b,t}(report.probeIndex)), DATA.centreLeftAdjusted, dev.centreScreenY, dev.white);
            DrawFormattedText(dev.wnd, cfg.attentionProbeID(DATA.trialDef.attentionalProbeShownID{b,t}(report.probeIndex)), DATA.centreRightAdjusted, dev.centreScreenY, dev.white);
            
        end
    end
    vbl = Screen('Flip', dev.wnd, vbl + (waitframes - 0.5) * ifi);
    
end

PsychHID('KbQueueStop', dev.inputKeyboardIndex);
PsychHID('KbQueueFlush', dev.inputKeyboardIndex);

end

