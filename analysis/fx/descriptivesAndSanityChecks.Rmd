### Mean(SD) percent correct
```{r GetMeanType1}
#Re-do summaryData after exclusion 
#Means first level
summary1stData <- rawData %>%
  select(correct,type1,contReport,suj) %>%
  group_by(type1,contReport,suj) %>%
  dplyr::summarise(meanCorrectType1 = mean(correct,na.rm=T))

#Means secondLevel
summary2ndData <- summary1stData[summary1stData$type1 == "R1+",] %>%
  group_by(contReport) %>%
  dplyr::summarise(groupMeanCorrectType1 = mean(meanCorrectType1),
                   groupSDCorrectType1 = sd(meanCorrectType1))

t.test(meanCorrectType1~contReport,paired=T,data=summary1stData[summary1stData$type1=='R1+',])
cohen.d(meanCorrectType1~contReport,paired=T,data=summary1stData[summary1stData$type1=='R1+',])
ttestBF(formula=meanCorrectType1~contReport,data=summary1stData[summary1stData$type1=='R1+',])

```


### Mean(SD) duration difference (converged staircase value)
```{r GetMeanType1b}
#Re-do summaryData after exclusion 
#Means first level
summary1stData <- rawData %>%
  select(durationDifference,type1,contReport,suj) %>%
  group_by(type1,contReport,suj) %>%
  dplyr::summarise(meanStaircaseValue = mean(durationDifference)) #getting the mean of the last 10 values (We check for 'convergence')


#Means secondLevel
summary2ndData <- summary1stData[summary1stData$type1 == "R1+",] %>%
  group_by(contReport) %>%
  dplyr::summarise(groupMeanStaircaseValue = mean(meanStaircaseValue),
                   groupSDStaircaseValue = sd(meanStaircaseValue))

t.test(meanStaircaseValue~contReport,paired=T,data=summary1stData[summary1stData$type1=='R1+',])
cohen.d(meanStaircaseValue~contReport,paired=T,data=summary1stData[summary1stData$type1=='R1+',])
ttestBF(formula=meanStaircaseValue~contReport,data=summary1stData[summary1stData$type1=='R1+',])


```


### Accuracy of the proxy
```{r getProxyQuality}
#Means first level
summary1stData <- rawData %>%
  filter(type1=="R1+", contReport == "CR+") %>% 
  group_by(suj) %>%
  dplyr::summarise(meanProxyCorrect = mean(predictionCorrect,na.rm=T))
#Print 
as.data.frame(summary1stData)
min(summary1stData$meanProxyCorrect)
max(summary1stData$meanProxyCorrect)
mean(summary1stData$meanProxyCorrect)
sd(summary1stData$meanProxyCorrect)
```


### RT1 predicts confidence?
```{r conf.vs.Type1RT}
bayes.conf.type1rt.full = brm(confChosen ~ type1RT * combiCondition + durationDifference + (type1RT + combiCondition | suj), data = data.withRT1, family = gaussian,  control = list(adapt_delta = 0.99), sample_prior=T, chains=chains, iter=iter, cores=cores, file=here::here('models','bayes.conf.type1rt.full')) # remove file to update model!
summary(bayes.conf.type1rt.full)

h <- c("main conf" = "type1RT  < 0",
       "main cond" = "combiConditionCRP_R1P  > 0",
       "main evidence" = "durationDifference > 0",
       "conf*cond" = "type1RT:combiConditionCRP_R1P  > 0")
(hyp=hypothesis(bayes.conf.type1rt.full, h))

plot.confVsRT.model = plot_model(bayes.conf.type1rt.full,
                                 type = "pred",
                                 terms=c('type1RT', 'combiCondition'), 
                                 alpha = 0.6) + 
  labs(fill = " ", x="First-order RT (s)", y="Confidence", title='R1+ conditions only', shape = 19) + 
  guides(fill = guide_legend(override.aes = list(shape = 19)), color = FALSE)
plot.confVsRT.model


```

Yes, as we expected, type1RT clearly predicts confidence (in the subset of trials with type1)



### Interaction RT1*accuracy predicts confidence?
```{r conf.vs.Type1RT.accuracy}
data.withRT1$correctFactor = factor(data.withRT1$correctFactor,levels=c('Correct','Incorrect','NaN'))

bayes.conf.type1rt.accuracy.full = brm(conf ~ type1RT * contReport * correctFactor + durationDifference + (type1RT + contReport + correctFactor + durationDifference | suj), data = data.withRT1, family = gaussian,  control = list(adapt_delta = 0.99), sample_prior=T, chains=chains, iter=iter, cores=cores, file=here::here('models','bayes.conf.type1rt.accuracy.full')) # remove file to update model!
summary(bayes.conf.type1rt.accuracy.full)

h <- c("main RT" = "type1RT < 0",
       "main cond" = "contReportCRP  < 0",
       "main cor" = "correctFactorIncorrect  < 0",
       "main evidence" = "durationDifference > 0",
       "conf*cond" = "type1RT:contReportCRP  < 0",
       "conf*cor" = "type1RT:correctFactorIncorrect  > 0", 
       "conf*cond*cor" = "type1RT:contReportCRP:correctFactorIncorrect > 0"
)
(hyp=hypothesis(bayes.conf.type1rt.accuracy.full, h))

data.withRT1$type1RT_round=factor(round(data.withRT1$type1RT))
roundconf = data.withRT1 %>% filter(!is.na(type1RT_round)) %>% group_by(type1RT_round,suj,correctFactor,contReport) %>% dplyr::summarise(conf=mean(conf)) %>% na.omit()
names(roundconf)[3] = 'facet'

plot.confVsRT.accuracy.model = plot_model(
  bayes.conf.type1rt.accuracy.full,
  type = "pred",show.data=F,
  terms=c('type1RT', 'contReport', 'correctFactor'))


plot.confVsRT.accuracy.model = plot.confVsRT.accuracy.model$data %>% as.data.frame() %>% 
  ggplot(aes(x,predicted,ymax=conf.high,ymin=conf.low,color=group,fill=group,linetype=group)) + geom_ribbon( alpha = 0.3,color=NA)+ geom_line()+
  scale_color_manual(values=mycols[c(2,4)]) + 
  scale_fill_manual(values=mycols[c(2,4)])+
  #scale_linetype_manual(values=c('solid','dashed','solid','dashed'))+
  scale_linetype_manual(values=c('solid','solid','solid','solid'))+
  stat_summary(data=roundconf,aes(x=as.numeric(type1RT_round)-.5, y=conf,color=contReport),inherit.aes = F,pch=21,fill='white',fun.data=mean_cl_normal,geom='pointrange',position=position_dodge(width=0.4),alpha=.5) +
  facet_wrap(~facet) +xlim(-0.3,4.3) +
  #labs(fill = " ", x="First-order RT (s)", y="Confidence",title='B. R+ conditions only') +
  labs(fill = " ", x="First-order RT (s)", y="Confidence",title='B. ') +
  guides(fill = guide_legend(override.aes = list(shape = 19)), color = FALSE,linetype=F) + theme(legend.position='bottom') 



figure2.B <- plot.confVsRT.accuracy.model 

figure2.B

```



### Mean confidence between conditions 
(1) Does Type1 as a factor (present/absent) affect confidence?
(2) Does ContReport as a factor (present/absent) affect confidence?
```{r MeanConfAllConditions}

conf.allConds.bayes = brm(conf ~ type1 * contReport + (type1+contReport|suj), data = rawData,  control = list(adapt_delta = 0.99), sample_prior=T, chains=chains, iter=iter, cores=cores, file=here::here('models','conf.allConds.bayes'))
summary(conf.allConds.bayes)

h <- c("main type1" = "type1R1P  > 0",
       "main contreport" = "contReportCRP  > 0",
       "main inter" = "type1R1P:contReportCRP > 0"
)
(hyp=hypothesis(conf.allConds.bayes, h))


conf.allConds.bayes



meanConfs <- rawData %>%
  group_by(suj,type1,contReport) %>%
  dplyr::summarise(meanConf = mean(conf,na.rm=T))

meanConf.allConds <- ggplot(meanConfs, aes(x=type1,y=meanConf,color=paste(contReport,type1),fill=contReport)) + 
  geom_boxplot(width=0.6,outlier.shape=NA,alpha=.3, color="black", aes(linetype=type1)) + 
  geom_dotplot(dotsize=.8,binaxis="y", stackdir="center",position = position_dodge(width=.6), show.legend=FALSE,fill='white',alpha=.8, aes(color=contReport)) +
  scale_color_manual(values=mycols[c(2,4)]) + 
  scale_linetype_manual(values=c("dashed", "solid"))+
  scale_x_discrete(labels=c("R-", "R+")) +
  scale_fill_manual(values=mycols[c(2,4)]) +
  labs(x='',y='Mean confidence', fill = " ") +
  ylim(0,1) + theme(legend.position='none')+
  ggtitle('A.')

figure2.A <- meanConf.allConds

figure2.A
```


No. responding to the type1 question alone does **NOT** add confidence. Yet providing a continuous report increases confidence.