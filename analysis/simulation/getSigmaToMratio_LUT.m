close all
clear all

rootpath = '~/git/noReport_metacognition/'
addpath('~/Dropbox/elisas_matlab_toolbxs/brians_metad/')
addpath([rootpath 'analysis_behave/fx/'])

ntrials = 1e7;
d = 1; %dprime
sigma = 1;

type1_c = 0;
mu_signal = d/2;
mu_noise = -d/2;

type2_c_signal = [.7];
type2_c_noise = [-.7];

nRatings = length(type2_c_signal) +1;
counter = 0;

sigma_added = -2: .1: 4;

for s_add = sigma_added 
    
    counter = counter + 1;
    sprintf('computing sigma added: %f', s_add)
    
    %select the stimulus
    data.stimulus = rand(ntrials,1)<0.5';
    
    % draw the evidence produced by 1 million simulation trials.
    data.x(data.stimulus)  = normrnd(mu_signal, sigma, sum(data.stimulus), 1);
    data.x(~data.stimulus) = normrnd(mu_noise, sigma, sum(~data.stimulus), 1);
    data.x = data.x(:);
    
    %degrade the signal
    data.x_degraded(data.stimulus)  = data.x(data.stimulus) + normrnd(0, abs(s_add), sum(data.stimulus), 1);
    data.x_degraded(~data.stimulus) = data.x(~data.stimulus) + normrnd(0, abs(s_add), sum(~data.stimulus), 1);
    data.x_degraded = data.x_degraded(:);

    [mu_degraded(counter), sigma_degraded(counter)] = normfit(data.x_degraded(data.stimulus));
    
    if s_add < 0
        data.x_type1 = data.x_degraded;
        data.x_type2 = data.x;
    else
        data.x_type1 = data.x;
        data.x_type2 = data.x_degraded;
    end
    
    % compute type 1 response
    data.R1 = data.x_type1>type1_c;
    
    % compute type 2 response    
    data.R2 = ones(size(data.x_type2));
    %For trials with answer = "signal"
    for c = 1:nRatings-1
        aboveType2C_1_signal{c} = find(data.R1 & data.x_type2 >= type2_c_signal(c));
        data.R2(aboveType2C_1_signal{c}) = c+1;
    end
    
    %For trials with answer = "noise"
    for c = 1:nRatings-1
        belowType2C_1_noise{c} = find(~data.R1 & data.x_type2 <= type2_c_noise(c));
        data.R2(belowType2C_1_noise{c}) = c+1;
    end
    data.R2 = data.R2(:);
    
    % get response vectors for metad'
    padCells = 0;
    [nR_S1, nR_S2] = trials2counts(data.stimulus, data.R1, data.R2, nRatings, padCells);
    out = type2_SDT_SSE(nR_S1, nR_S2);
    
    Mratio(counter) = out.M_ratio;
    
end

figure;
plot(sigma_added, Mratio, '+')
xlabel('sigma added. Negative is added to type1')
ylabel('Mratio')
ylim([0 1.3])
realEstimate_Hmetad = [.1 .2];
hline(realEstimate_Hmetad,'k'); 

figure;
plot(sigma_added, sigma_degraded, '+-')
xlim([0 max(sigma_added)]) %ignore the (nonsensical and symmetrical) negative sigma_added
xlabel('sigma added')
ylabel('Effective sigma of the degraded distribution')

sigma_degraded(sigma_added==0)

save([rootpath '/resultData/sigma_2_Mratio_LUT.mat'], 'Mratio', 'sigma_added')


