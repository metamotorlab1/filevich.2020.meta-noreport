**Contains experimental protocol, data, and analysis scripts for:**


Elisa Filevich, Christina Koß, and Nathan Faivre (2019). Response-related signals increase confidence but not metacognitive performance. 

https://www.eneuro.org/content/7/3/ENEURO.0326-19.2020

This repo copied from https://gitlab.com/nfaivre/filevich_metareport
